import { NotificationService } from './../../notifications/notification.service';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  hide = true;
  signinFormGroup!: FormGroup;
  loginSubmitted!: boolean;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.loginFormValidation();
  }

  //====================== sign in form validation =================
  loginFormValidation() {
    this.signinFormGroup = this._formBuilder.group({
      username: ['John@gmail.com', [Validators.required]],
      password: ['rules', [Validators.required]],
    });
  }

  get loginFn() {
    return this.signinFormGroup.controls;
  }

  loginSubmitData() {
    this.loginSubmitted = true;
    if (
      this.signinFormGroup.get('username')?.value === 'John@gmail.com' &&
      this.signinFormGroup.get('password')?.value === 'rules'
    ) {
      let user = 'Basic U01FX0xlbmRpbmdfQVBJOlJ1bGVzQDEyMzQ1';

      const data = {
        name: this.signinFormGroup.get('username')?.value,
        pass: this.signinFormGroup.get('password')?.value,
      };
      let IsToken = {
        login: true,
      };

      this.authService.sendToken(user);
      localStorage.setItem('users', JSON.stringify(data));
      this.router.navigate(['/home/dashboard']);
    } else {
      this.notificationService.showError(
        'username or password wrong',
        'Sorry!'
      );
      this.router.navigate(['/login']);
    }
  }
}

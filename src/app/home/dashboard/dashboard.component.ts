import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Owner } from 'src/app/models/owner';
import { NotificationService } from 'src/app/notifications/notification.service';
import { LoanService } from 'src/app/services/loan.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public displayedColumns = [
    'name',
    'id',
    'urgency',
    'status',
    'details',
    'update',
    'delete',
  ];
  public dataSource = new MatTableDataSource<Owner>();

  @ViewChild(MatSort)
  sort!: MatSort;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(
    private loanService: LoanService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.getAllOwners();
  }

  public getAllOwners = () => {
    this.loanService.getAllCases().subscribe((res: any) => {
      console.log(res.body.cases);
      if (res.status === 200) {
        res.body.cases.forEach((element: any) => {
          let str = element.ID;
          let modifiycaseName = str.substring(str.lastIndexOf(' ') + 1);
          element.modifiycaseName = modifiycaseName;
        });
        this.dataSource.data = res.body.cases as Owner[];
      } else {
        // console.log(res)
      }
    });
  };

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort = (event: any) => {
    console.log(event);
  };

  public doFilter = (event: any) => {
    console.log(event.target.value);
    this.dataSource.filter = event.target.value.trim().toLocaleLowerCase();
  };

  public redirectToDetails = (id: string) => {};

  public redirectToUpdate = (id: string) => {};

  public redirectToDelete = (id: string) => {};
}

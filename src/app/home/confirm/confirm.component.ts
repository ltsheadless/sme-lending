import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css'],
})
export class ConfirmComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}
  caseName: any;
  ngOnInit(): void {
    this.caseName = this.route.snapshot.params['caseId'];
  }
}

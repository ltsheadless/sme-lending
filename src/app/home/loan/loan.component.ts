import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { NotificationService } from 'src/app/notifications/notification.service';
import { LoanService } from 'src/app/services/loan.service';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css'],
})
export class LoanComponent implements OnInit {
  isLinear = true;
  step: any;
  profileFormGroup!: FormGroup;
  companyFormGroup!: FormGroup;
  loanFormGroup!: FormGroup;
  calculatorFormGroup!: FormGroup;
  profileSubmitted!: boolean;
  companySubmitted!: boolean;
  loanSubmitted!: boolean;
  calculatorSubmitted!: boolean;
  showBankExcutiveHelp = false;

  formattedAmount_la: any;
  formattedAmount_cv: any;

  fileName: any;
  caseId: any;

  //================== Gauage==========================//
  public canvasWidth = 250;
  public needleValue = 12;
  public centralLabel = '';
  public name = 'Loan Calculator';
  public bottomLabel: any;

  public options = {
    hasNeedle: true,
    needleColor: 'black',
    needleStartValue: 0,
    arcColors: [
      'rgb(119,107,10)',
      'rgb(196,250,244)',
      'rgb(115,179,14)',
      'rgb(239,214,19)',
    ],
    arcDelimiters: [60, 75, 83, 99],
    rangeLabel: ['Low', 'High'],
  };

  noEmployees = [
    { value: '1-50', viewValue: '1-50' },
    { value: '51-100', viewValue: '51-100' },
    { value: '101-200', viewValue: '101-200' },
    { value: '201-500', viewValue: '201-500' },
    { value: '501-1000', viewValue: '501-1000' },
    { value: '1000+', viewValue: '1000+' },
  ];

  typeBuisness = [
    { value: 'Trading', viewValue: 'Trading' },
    { value: 'Manufacturing', viewValue: 'Manufacturing' },
    { value: 'Consulting', viewValue: 'Consulting' },
  ];
  // locations = [
  //   { value: 'Copenhagen', viewValue: 'Copenhagen' },
  //   { value: 'Stockholm', viewValue: 'Stockholm' },
  //   { value: 'Aarhus', viewValue: 'Aarhus' },
  //   { value: 'Aalborg', viewValue: 'Aalborg' },
  //   { value: 'Odense', viewValue: 'Odense' },
  //   { value: 'Roskilde', viewValue: 'Roskilde' },
  //   { value: 'Esbjerg', viewValue: 'Esbjerg' },
  //   { value: 'Vejle', viewValue: 'Vejle' },
  //   { value: 'Kolding', viewValue: 'Kolding' },
  //   { value: 'Randers', viewValue: 'Randers' },
  // ];
  sectores = [
    { value: 'Agriculture', viewValue: 'Agriculture' },
    { value: 'Pharma', viewValue: 'Pharma' },
    { value: 'Steel', viewValue: 'Steel' },
    { value: 'Food and Beverages', viewValue: 'Food and Beverages' },
    { value: 'Real Estate', viewValue: 'Real Estate' },
    { value: 'Technology', viewValue: 'Technology' },
    { value: 'Textiles', viewValue: 'Textiles' },
    { value: 'Logistics', viewValue: 'Logistics' },
    { value: 'Entertainment', viewValue: 'Entertainment' },
    { value: 'Banking', viewValue: 'Banking' },
  ];

  locations = [
    { code: 'AF', code3: 'AFG', name: 'Afghanistan', number: '004' },
    { code: 'AL', code3: 'ALB', name: 'Albania', number: '008' },
    { code: 'DZ', code3: 'DZA', name: 'Algeria', number: '012' },
    { code: 'AS', code3: 'ASM', name: 'American Samoa', number: '016' },
    { code: 'AD', code3: 'AND', name: 'Andorra', number: '020' },
    { code: 'AO', code3: 'AGO', name: 'Angola', number: '024' },
    { code: 'AI', code3: 'AIA', name: 'Anguilla', number: '660' },
    { code: 'AQ', code3: 'ATA', name: 'Antarctica', number: '010' },
    { code: 'AG', code3: 'ATG', name: 'Antigua and Barbuda', number: '028' },
    { code: 'AR', code3: 'ARG', name: 'Argentina', number: '032' },
    { code: 'AM', code3: 'ARM', name: 'Armenia', number: '051' },
    { code: 'AW', code3: 'ABW', name: 'Aruba', number: '533' },
    { code: 'AU', code3: 'AUS', name: 'Australia', number: '036' },
    { code: 'AT', code3: 'AUT', name: 'Austria', number: '040' },
    { code: 'AZ', code3: 'AZE', name: 'Azerbaijan', number: '031' },
    { code: 'BS', code3: 'BHS', name: 'Bahamas (the)', number: '044' },
    { code: 'BH', code3: 'BHR', name: 'Bahrain', number: '048' },
    { code: 'BD', code3: 'BGD', name: 'Bangladesh', number: '050' },
    { code: 'BB', code3: 'BRB', name: 'Barbados', number: '052' },
    { code: 'BY', code3: 'BLR', name: 'Belarus', number: '112' },
    { code: 'BE', code3: 'BEL', name: 'Belgium', number: '056' },
    { code: 'BZ', code3: 'BLZ', name: 'Belize', number: '084' },
    { code: 'BJ', code3: 'BEN', name: 'Benin', number: '204' },
    { code: 'BM', code3: 'BMU', name: 'Bermuda', number: '060' },
    { code: 'BT', code3: 'BTN', name: 'Bhutan', number: '064' },
    {
      code: 'BO',
      code3: 'BOL',
      name: 'Bolivia (Plurinational State of)',
      number: '068',
    },
    {
      code: 'BQ',
      code3: 'BES',
      name: 'Bonaire, Sint Eustatius and Saba',
      number: '535',
    },
    { code: 'BA', code3: 'BIH', name: 'Bosnia and Herzegovina', number: '070' },
    { code: 'BW', code3: 'BWA', name: 'Botswana', number: '072' },
    { code: 'BV', code3: 'BVT', name: 'Bouvet Island', number: '074' },
    { code: 'BR', code3: 'BRA', name: 'Brazil', number: '076' },
    {
      code: 'IO',
      code3: 'IOT',
      name: 'British Indian Ocean Territory (the)',
      number: '086',
    },
    { code: 'BN', code3: 'BRN', name: 'Brunei Darussalam', number: '096' },
    { code: 'BG', code3: 'BGR', name: 'Bulgaria', number: '100' },
    { code: 'BF', code3: 'BFA', name: 'Burkina Faso', number: '854' },
    { code: 'BI', code3: 'BDI', name: 'Burundi', number: '108' },
    { code: 'CV', code3: 'CPV', name: 'Cabo Verde', number: '132' },
    { code: 'KH', code3: 'KHM', name: 'Cambodia', number: '116' },
    { code: 'CM', code3: 'CMR', name: 'Cameroon', number: '120' },
    { code: 'CA', code3: 'CAN', name: 'Canada', number: '124' },
    { code: 'KY', code3: 'CYM', name: 'Cayman Islands (the)', number: '136' },
    {
      code: 'CF',
      code3: 'CAF',
      name: 'Central African Republic (the)',
      number: '140',
    },
    { code: 'TD', code3: 'TCD', name: 'Chad', number: '148' },
    { code: 'CL', code3: 'CHL', name: 'Chile', number: '152' },
    { code: 'CN', code3: 'CHN', name: 'China', number: '156' },
    { code: 'CX', code3: 'CXR', name: 'Christmas Island', number: '162' },
    {
      code: 'CC',
      code3: 'CCK',
      name: 'Cocos (Keeling) Islands (the)',
      number: '166',
    },
    { code: 'CO', code3: 'COL', name: 'Colombia', number: '170' },
    { code: 'KM', code3: 'COM', name: 'Comoros (the)', number: '174' },
    {
      code: 'CD',
      code3: 'COD',
      name: 'Congo (the Democratic Republic of the)',
      number: '180',
    },
    { code: 'CG', code3: 'COG', name: 'Congo (the)', number: '178' },
    { code: 'CK', code3: 'COK', name: 'Cook Islands (the)', number: '184' },
    { code: 'CR', code3: 'CRI', name: 'Costa Rica', number: '188' },
    { code: 'HR', code3: 'HRV', name: 'Croatia', number: '191' },
    { code: 'CU', code3: 'CUB', name: 'Cuba', number: '192' },
    { code: 'CW', code3: 'CUW', name: 'Curaçao', number: '531' },
    { code: 'CY', code3: 'CYP', name: 'Cyprus', number: '196' },
    { code: 'CZ', code3: 'CZE', name: 'Czechia', number: '203' },
    { code: 'CI', code3: 'CIV', name: "Côte d'Ivoire", number: '384' },
    { code: 'DK', code3: 'DNK', name: 'Denmark', number: '208' },
    { code: 'DJ', code3: 'DJI', name: 'Djibouti', number: '262' },
    { code: 'DM', code3: 'DMA', name: 'Dominica', number: '212' },
    {
      code: 'DO',
      code3: 'DOM',
      name: 'Dominican Republic (the)',
      number: '214',
    },
    { code: 'EC', code3: 'ECU', name: 'Ecuador', number: '218' },
    { code: 'EG', code3: 'EGY', name: 'Egypt', number: '818' },
    { code: 'SV', code3: 'SLV', name: 'El Salvador', number: '222' },
    { code: 'GQ', code3: 'GNQ', name: 'Equatorial Guinea', number: '226' },
    { code: 'ER', code3: 'ERI', name: 'Eritrea', number: '232' },
    { code: 'EE', code3: 'EST', name: 'Estonia', number: '233' },
    { code: 'SZ', code3: 'SWZ', name: 'Eswatini', number: '748' },
    { code: 'ET', code3: 'ETH', name: 'Ethiopia', number: '231' },
    {
      code: 'FK',
      code3: 'FLK',
      name: 'Falkland Islands (the) [Malvinas]',
      number: '238',
    },
    { code: 'FO', code3: 'FRO', name: 'Faroe Islands (the)', number: '234' },
    { code: 'FJ', code3: 'FJI', name: 'Fiji', number: '242' },
    { code: 'FI', code3: 'FIN', name: 'Finland', number: '246' },
    { code: 'FR', code3: 'FRA', name: 'France', number: '250' },
    { code: 'GF', code3: 'GUF', name: 'French Guiana', number: '254' },
    { code: 'PF', code3: 'PYF', name: 'French Polynesia', number: '258' },
    {
      code: 'TF',
      code3: 'ATF',
      name: 'French Southern Territories (the)',
      number: '260',
    },
    { code: 'GA', code3: 'GAB', name: 'Gabon', number: '266' },
    { code: 'GM', code3: 'GMB', name: 'Gambia (the)', number: '270' },
    { code: 'GE', code3: 'GEO', name: 'Georgia', number: '268' },
    { code: 'DE', code3: 'DEU', name: 'Germany', number: '276' },
    { code: 'GH', code3: 'GHA', name: 'Ghana', number: '288' },
    { code: 'GI', code3: 'GIB', name: 'Gibraltar', number: '292' },
    { code: 'GR', code3: 'GRC', name: 'Greece', number: '300' },
    { code: 'GL', code3: 'GRL', name: 'Greenland', number: '304' },
    { code: 'GD', code3: 'GRD', name: 'Grenada', number: '308' },
    { code: 'GP', code3: 'GLP', name: 'Guadeloupe', number: '312' },
    { code: 'GU', code3: 'GUM', name: 'Guam', number: '316' },
    { code: 'GT', code3: 'GTM', name: 'Guatemala', number: '320' },
    { code: 'GG', code3: 'GGY', name: 'Guernsey', number: '831' },
    { code: 'GN', code3: 'GIN', name: 'Guinea', number: '324' },
    { code: 'GW', code3: 'GNB', name: 'Guinea-Bissau', number: '624' },
    { code: 'GY', code3: 'GUY', name: 'Guyana', number: '328' },
    { code: 'HT', code3: 'HTI', name: 'Haiti', number: '332' },
    {
      code: 'HM',
      code3: 'HMD',
      name: 'Heard Island and McDonald Islands',
      number: '334',
    },
    { code: 'VA', code3: 'VAT', name: 'Holy See (the)', number: '336' },
    { code: 'HN', code3: 'HND', name: 'Honduras', number: '340' },
    { code: 'HK', code3: 'HKG', name: 'Hong Kong', number: '344' },
    { code: 'HU', code3: 'HUN', name: 'Hungary', number: '348' },
    { code: 'IS', code3: 'ISL', name: 'Iceland', number: '352' },
    { code: 'IN', code3: 'IND', name: 'India', number: '356' },
    { code: 'ID', code3: 'IDN', name: 'Indonesia', number: '360' },
    {
      code: 'IR',
      code3: 'IRN',
      name: 'Iran (Islamic Republic of)',
      number: '364',
    },
    { code: 'IQ', code3: 'IRQ', name: 'Iraq', number: '368' },
    { code: 'IE', code3: 'IRL', name: 'Ireland', number: '372' },
    { code: 'IM', code3: 'IMN', name: 'Isle of Man', number: '833' },
    { code: 'IL', code3: 'ISR', name: 'Israel', number: '376' },
    { code: 'IT', code3: 'ITA', name: 'Italy', number: '380' },
    { code: 'JM', code3: 'JAM', name: 'Jamaica', number: '388' },
    { code: 'JP', code3: 'JPN', name: 'Japan', number: '392' },
    { code: 'JE', code3: 'JEY', name: 'Jersey', number: '832' },
    { code: 'JO', code3: 'JOR', name: 'Jordan', number: '400' },
    { code: 'KZ', code3: 'KAZ', name: 'Kazakhstan', number: '398' },
    { code: 'KE', code3: 'KEN', name: 'Kenya', number: '404' },
    { code: 'KI', code3: 'KIR', name: 'Kiribati', number: '296' },
    {
      code: 'KP',
      code3: 'PRK',
      name: "Korea (the Democratic People's Republic of)",
      number: '408',
    },
    {
      code: 'KR',
      code3: 'KOR',
      name: 'Korea (the Republic of)',
      number: '410',
    },
    { code: 'KW', code3: 'KWT', name: 'Kuwait', number: '414' },
    { code: 'KG', code3: 'KGZ', name: 'Kyrgyzstan', number: '417' },
    {
      code: 'LA',
      code3: 'LAO',
      name: "Lao People's Democratic Republic (the)",
      number: '418',
    },
    { code: 'LV', code3: 'LVA', name: 'Latvia', number: '428' },
    { code: 'LB', code3: 'LBN', name: 'Lebanon', number: '422' },
    { code: 'LS', code3: 'LSO', name: 'Lesotho', number: '426' },
    { code: 'LR', code3: 'LBR', name: 'Liberia', number: '430' },
    { code: 'LY', code3: 'LBY', name: 'Libya', number: '434' },
    { code: 'LI', code3: 'LIE', name: 'Liechtenstein', number: '438' },
    { code: 'LT', code3: 'LTU', name: 'Lithuania', number: '440' },
    { code: 'LU', code3: 'LUX', name: 'Luxembourg', number: '442' },
    { code: 'MO', code3: 'MAC', name: 'Macao', number: '446' },
    { code: 'MG', code3: 'MDG', name: 'Madagascar', number: '450' },
    { code: 'MW', code3: 'MWI', name: 'Malawi', number: '454' },
    { code: 'MY', code3: 'MYS', name: 'Malaysia', number: '458' },
    { code: 'MV', code3: 'MDV', name: 'Maldives', number: '462' },
    { code: 'ML', code3: 'MLI', name: 'Mali', number: '466' },
    { code: 'MT', code3: 'MLT', name: 'Malta', number: '470' },
    { code: 'MH', code3: 'MHL', name: 'Marshall Islands (the)', number: '584' },
    { code: 'MQ', code3: 'MTQ', name: 'Martinique', number: '474' },
    { code: 'MR', code3: 'MRT', name: 'Mauritania', number: '478' },
    { code: 'MU', code3: 'MUS', name: 'Mauritius', number: '480' },
    { code: 'YT', code3: 'MYT', name: 'Mayotte', number: '175' },
    { code: 'MX', code3: 'MEX', name: 'Mexico', number: '484' },
    {
      code: 'FM',
      code3: 'FSM',
      name: 'Micronesia (Federated States of)',
      number: '583',
    },
    {
      code: 'MD',
      code3: 'MDA',
      name: 'Moldova (the Republic of)',
      number: '498',
    },
    { code: 'MC', code3: 'MCO', name: 'Monaco', number: '492' },
    { code: 'MN', code3: 'MNG', name: 'Mongolia', number: '496' },
    { code: 'ME', code3: 'MNE', name: 'Montenegro', number: '499' },
    { code: 'MS', code3: 'MSR', name: 'Montserrat', number: '500' },
    { code: 'MA', code3: 'MAR', name: 'Morocco', number: '504' },
    { code: 'MZ', code3: 'MOZ', name: 'Mozambique', number: '508' },
    { code: 'MM', code3: 'MMR', name: 'Myanmar', number: '104' },
    { code: 'NA', code3: 'NAM', name: 'Namibia', number: '516' },
    { code: 'NR', code3: 'NRU', name: 'Nauru', number: '520' },
    { code: 'NP', code3: 'NPL', name: 'Nepal', number: '524' },
    { code: 'NL', code3: 'NLD', name: 'Netherlands (the)', number: '528' },
    { code: 'NC', code3: 'NCL', name: 'New Caledonia', number: '540' },
    { code: 'NZ', code3: 'NZL', name: 'New Zealand', number: '554' },
    { code: 'NI', code3: 'NIC', name: 'Nicaragua', number: '558' },
    { code: 'NE', code3: 'NER', name: 'Niger (the)', number: '562' },
    { code: 'NG', code3: 'NGA', name: 'Nigeria', number: '566' },
    { code: 'NU', code3: 'NIU', name: 'Niue', number: '570' },
    { code: 'NF', code3: 'NFK', name: 'Norfolk Island', number: '574' },
    {
      code: 'MP',
      code3: 'MNP',
      name: 'Northern Mariana Islands (the)',
      number: '580',
    },
    { code: 'NO', code3: 'NOR', name: 'Norway', number: '578' },
    { code: 'OM', code3: 'OMN', name: 'Oman', number: '512' },
    { code: 'PK', code3: 'PAK', name: 'Pakistan', number: '586' },
    { code: 'PW', code3: 'PLW', name: 'Palau', number: '585' },
    { code: 'PS', code3: 'PSE', name: 'Palestine, State of', number: '275' },
    { code: 'PA', code3: 'PAN', name: 'Panama', number: '591' },
    { code: 'PG', code3: 'PNG', name: 'Papua New Guinea', number: '598' },
    { code: 'PY', code3: 'PRY', name: 'Paraguay', number: '600' },
    { code: 'PE', code3: 'PER', name: 'Peru', number: '604' },
    { code: 'PH', code3: 'PHL', name: 'Philippines (the)', number: '608' },
    { code: 'PN', code3: 'PCN', name: 'Pitcairn', number: '612' },
    { code: 'PL', code3: 'POL', name: 'Poland', number: '616' },
    { code: 'PT', code3: 'PRT', name: 'Portugal', number: '620' },
    { code: 'PR', code3: 'PRI', name: 'Puerto Rico', number: '630' },
    { code: 'QA', code3: 'QAT', name: 'Qatar', number: '634' },
    {
      code: 'MK',
      code3: 'MKD',
      name: 'Republic of North Macedonia',
      number: '807',
    },
    { code: 'RO', code3: 'ROU', name: 'Romania', number: '642' },
    {
      code: 'RU',
      code3: 'RUS',
      name: 'Russian Federation (the)',
      number: '643',
    },
    { code: 'RW', code3: 'RWA', name: 'Rwanda', number: '646' },
    { code: 'RE', code3: 'REU', name: 'Réunion', number: '638' },
    { code: 'BL', code3: 'BLM', name: 'Saint Barthélemy', number: '652' },
    {
      code: 'SH',
      code3: 'SHN',
      name: 'Saint Helena, Ascension and Tristan da Cunha',
      number: '654',
    },
    { code: 'KN', code3: 'KNA', name: 'Saint Kitts and Nevis', number: '659' },
    { code: 'LC', code3: 'LCA', name: 'Saint Lucia', number: '662' },
    {
      code: 'MF',
      code3: 'MAF',
      name: 'Saint Martin (French part)',
      number: '663',
    },
    {
      code: 'PM',
      code3: 'SPM',
      name: 'Saint Pierre and Miquelon',
      number: '666',
    },
    {
      code: 'VC',
      code3: 'VCT',
      name: 'Saint Vincent and the Grenadines',
      number: '670',
    },
    { code: 'WS', code3: 'WSM', name: 'Samoa', number: '882' },
    { code: 'SM', code3: 'SMR', name: 'San Marino', number: '674' },
    { code: 'ST', code3: 'STP', name: 'Sao Tome and Principe', number: '678' },
    { code: 'SA', code3: 'SAU', name: 'Saudi Arabia', number: '682' },
    { code: 'SN', code3: 'SEN', name: 'Senegal', number: '686' },
    { code: 'RS', code3: 'SRB', name: 'Serbia', number: '688' },
    { code: 'SC', code3: 'SYC', name: 'Seychelles', number: '690' },
    { code: 'SL', code3: 'SLE', name: 'Sierra Leone', number: '694' },
    { code: 'SG', code3: 'SGP', name: 'Singapore', number: '702' },
    {
      code: 'SX',
      code3: 'SXM',
      name: 'Sint Maarten (Dutch part)',
      number: '534',
    },
    { code: 'SK', code3: 'SVK', name: 'Slovakia', number: '703' },
    { code: 'SI', code3: 'SVN', name: 'Slovenia', number: '705' },
    { code: 'SB', code3: 'SLB', name: 'Solomon Islands', number: '090' },
    { code: 'SO', code3: 'SOM', name: 'Somalia', number: '706' },
    { code: 'ZA', code3: 'ZAF', name: 'South Africa', number: '710' },
    {
      code: 'GS',
      code3: 'SGS',
      name: 'South Georgia and the South Sandwich Islands',
      number: '239',
    },
    { code: 'SS', code3: 'SSD', name: 'South Sudan', number: '728' },
    { code: 'ES', code3: 'ESP', name: 'Spain', number: '724' },
    { code: 'LK', code3: 'LKA', name: 'Sri Lanka', number: '144' },
    { code: 'SD', code3: 'SDN', name: 'Sudan (the)', number: '729' },
    { code: 'SR', code3: 'SUR', name: 'Suriname', number: '740' },
    { code: 'SJ', code3: 'SJM', name: 'Svalbard and Jan Mayen', number: '744' },
    { code: 'SE', code3: 'SWE', name: 'Sweden', number: '752' },
    { code: 'CH', code3: 'CHE', name: 'Switzerland', number: '756' },
    { code: 'SY', code3: 'SYR', name: 'Syrian Arab Republic', number: '760' },
    { code: 'TW', code3: 'TWN', name: 'Taiwan', number: '158' },
    { code: 'TJ', code3: 'TJK', name: 'Tajikistan', number: '762' },
    {
      code: 'TZ',
      code3: 'TZA',
      name: 'Tanzania, United Republic of',
      number: '834',
    },
    { code: 'TH', code3: 'THA', name: 'Thailand', number: '764' },
    { code: 'TL', code3: 'TLS', name: 'Timor-Leste', number: '626' },
    { code: 'TG', code3: 'TGO', name: 'Togo', number: '768' },
    { code: 'TK', code3: 'TKL', name: 'Tokelau', number: '772' },
    { code: 'TO', code3: 'TON', name: 'Tonga', number: '776' },
    { code: 'TT', code3: 'TTO', name: 'Trinidad and Tobago', number: '780' },
    { code: 'TN', code3: 'TUN', name: 'Tunisia', number: '788' },
    { code: 'TR', code3: 'TUR', name: 'Turkey', number: '792' },
    { code: 'TM', code3: 'TKM', name: 'Turkmenistan', number: '795' },
    {
      code: 'TC',
      code3: 'TCA',
      name: 'Turks and Caicos Islands (the)',
      number: '796',
    },
    { code: 'TV', code3: 'TUV', name: 'Tuvalu', number: '798' },
    { code: 'UG', code3: 'UGA', name: 'Uganda', number: '800' },
    { code: 'UA', code3: 'UKR', name: 'Ukraine', number: '804' },
    {
      code: 'AE',
      code3: 'ARE',
      name: 'United Arab Emirates (the)',
      number: '784',
    },
    {
      code: 'GB',
      code3: 'GBR',
      name: 'United Kingdom of Great Britain and Northern Ireland (the)',
      number: '826',
    },
    {
      code: 'UM',
      code3: 'UMI',
      name: 'United States Minor Outlying Islands (the)',
      number: '581',
    },
    {
      code: 'US',
      code3: 'USA',
      name: 'United States of America (the)',
      number: '840',
    },
    { code: 'UY', code3: 'URY', name: 'Uruguay', number: '858' },
    { code: 'UZ', code3: 'UZB', name: 'Uzbekistan', number: '860' },
    { code: 'VU', code3: 'VUT', name: 'Vanuatu', number: '548' },
    {
      code: 'VE',
      code3: 'VEN',
      name: 'Venezuela (Bolivarian Republic of)',
      number: '862',
    },
    { code: 'VN', code3: 'VNM', name: 'Viet Nam', number: '704' },
    {
      code: 'VG',
      code3: 'VGB',
      name: 'Virgin Islands (British)',
      number: '092',
    },
    { code: 'VI', code3: 'VIR', name: 'Virgin Islands (U.S.)', number: '850' },
    { code: 'WF', code3: 'WLF', name: 'Wallis and Futuna', number: '876' },
    { code: 'EH', code3: 'ESH', name: 'Western Sahara', number: '732' },
    { code: 'YE', code3: 'YEM', name: 'Yemen', number: '887' },
    { code: 'ZM', code3: 'ZMB', name: 'Zambia', number: '894' },
    { code: 'ZW', code3: 'ZWE', name: 'Zimbabwe', number: '716' },
    { code: 'AX', code3: 'ALA', name: 'Åland Islands', number: '248' },
  ];

  years = [
    { value: '2019', viewValue: '2019' },
    { value: '2020', viewValue: '2020' },
    { value: '2021', viewValue: '2021' },
  ];

  loanTypes = [
    { value: 'Secured', viewValue: 'Secured' },
    { value: 'Unsecured', viewValue: 'Unsecured' },
  ];

  loanPurposes = [
    { value: 'Real Estate', viewValue: 'Real Estate' },
    { value: 'Working Capital', viewValue: 'Working Capital' },
  ];

  collateralTypes = [
    { value: 'Account Receivables', viewValue: 'Account Receivables' },
    { value: 'Equipment', viewValue: 'Equipment' },
    { value: 'Personal Guarantee', viewValue: 'Personal Guarantee' },
    {
      value: 'Real Estate(property lien)',
      viewValue: 'Real Estate(property lien)',
    },
  ];
  alternateDatas: any;
  decisionnDatas: any;
  assetsDetails: any;
  netincomeDetails: any;
  liabilitiesDetails: any;
  loandataDetails: any;
  showALT!: boolean;
  bannerCaseName: any;
  selectDisable!: boolean;
  fileDialogShouldOpen = false;
  nxtBtnShowafterPM!: boolean;
  loanApprovedMsg!: boolean;
  finishBtnApproved!: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loanService: LoanService,
    private notificationService: NotificationService,
    private spinner: NgxSpinnerService
  ) {}

  @ViewChild('stepper')
  stepper!: MatStepper;

  ngOnInit() {
    this.getLoanDetailsByCaseID();

    let str = this.route.snapshot.params['caseId'];
    this.bannerCaseName = str.substring(str.lastIndexOf(' ') + 1);

    //===============profile=========================
    this.profileFormGroup = this._formBuilder.group({
      companyName: ['', Validators.required],
      isExistingCustomer: ['No'],
      address: ['', Validators.required],
      incorporationAddress: ['', Validators.required],
      title: ['mr.'],
      contactName: ['', [Validators.required]],
      contactNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      emailAddress: ['', [Validators.required, Validators.email]],
    });

    //===============company======================
    this.companyFormGroup = this._formBuilder.group({
      companyType: ['Public', Validators.required],
      buisnessType: ['', Validators.required],
      sector: ['', Validators.required],
      employeeNumber: ['', Validators.required],
      location: ['', Validators.required],
      profitloses: this._formBuilder.array([]),
      assets: this._formBuilder.array([]),
      liabilities: this._formBuilder.array([]),
    });

    //====================loan==============================
    this.loanFormGroup = this._formBuilder.group({
      loanAmount: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      loanType: ['', Validators.required],
      loanPurpose: ['', Validators.required],
      loanTenure: [
        '',
        [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
      ],
      isLoanCollaterized: ['No', Validators.required],
      collateralType: ['', Validators.required],
      checkLoanPeriod: false,
      collateralValue: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      filename: ['', Validators.required],
    });

    //=====================calculator=======================
    this.calculatorFormGroup = this._formBuilder.group({
      loanPurpose: ['', Validators.required],
      requestedLoanAmount: ['', Validators.required],
      collateralValue: ['', Validators.required],
      Decision: '',
      DecisionCode: '',
      Feedback: '',
      Score: '',
      alt_Alternative: '',
      alt_AlternativeValue: '',
      alt_Decision: '',
      alt_DecisionCode: '',
      alt_Feedback: '',
      alt_NumberOfAlternatives: '',
      alt1_Alternative: '',
      alt1_AlternativeValue: '',
      alt1_Decision: '',
      alt1_DecisionCode: '',
      alt1_Feedback: '',
      alt1_NumberOfAlternatives: '',
    });
  }

  goPreviousStep(n: number) {
    this.step = n;
    setTimeout(() => (this.stepper.selectedIndex = n - 1));
  }

  getLoanDetailsByCaseID() {
    this.caseId = this.route.snapshot.params['caseId'];

    this.loanService
      .getCaseById(this.route.snapshot.params['caseId'])
      .subscribe(
        (data) => {
          console.log(data);
          if (data.status === 200) {
            localStorage.removeItem('etag');
            localStorage.setItem('etag', data.headers.get('etag'));
            this.showALT = true;
            if (data.body.content.pxCurrentStage != 'PRIM2') {
              this.nxtBtnShowafterPM = true;
            } else {
              this.nxtBtnShowafterPM = false;
            }
             
            if ( data.body.content.ApplicationDetails.CreditAssessmentResultList != undefined) {
               
              if (data.body.content.ApplicationDetails.CreditAssessmentResultList[0].DecisionCode=='A' || data.body.content.ApplicationDetails.CreditAssessmentResultList[0].DecisionCode=='AC') {
                this.loanApprovedMsg = true
              this.finishBtnApproved = true
              } else {
                this.loanApprovedMsg = false
                this.finishBtnApproved = false
              }
             
            } else {
              
            }

            this.decisionnDatas =
              data.body.content.ApplicationDetails.CreditAssessmentResultList;
            this.alternateDatas =
              data.body.content.ApplicationDetails.CreditAlternativeResultList;
            this.loandataDetails = data.body.content;
            this.assetsDetails =
              data.body.content.ApplicationDetails.Customer.BalanceSheetAssets;
            this.netincomeDetails =
              data.body.content.ApplicationDetails.Customer.ProfitAndLoss;
            this.liabilitiesDetails =
              data.body.content.ApplicationDetails.Customer.BalanceSheetLiabilities;
            if (this.decisionnDatas === undefined) {
              //  let needleVal = parseInt(this.decisionnDatas[0].Score)/10
              this.needleValue = 43.5;
            } else {
              let needleVal = parseInt(this.decisionnDatas[0].Score) / 10;
              this.needleValue = needleVal;
            }

            this.profileDetailsFormValidation(data);
            this.companyDetailsFormValidation(data);
            this.loanDetailsFormValidation(data);
            this.calculatorDetailsFormValidation(data);
          }
        },
        (error) => {
          this.notificationService.showError(
            `${error.message}`,
            'error Occuring'
          );
        }
      );
  }

  //=================== start profile ================================
  profileDetailsFormValidation(data: any) {
    if (data.body.content.pxCurrentStage != 'PRIM2') {
      this.selectDisable = true;
      this.profileFormGroup = this._formBuilder.group({
        companyName: [
          data.body.content.ApplicationDetails.Customer.FullName === undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.FullName === null
            ? ''
            : data.body.content.ApplicationDetails.Customer.FullName === ''
            ? ''
            : {
                value: data.body.content.ApplicationDetails.Customer.FullName,
                disabled: true,
              },
          Validators.required,
        ],
        isExistingCustomer: [
          data.body.content.ApplicationDetails.Customer.ExistingCustomer ===
          undefined
            ? 'No'
            : data.body.content.ApplicationDetails.Customer.ExistingCustomer ===
              null
            ? 'no'
            : data.isExistingCustomer === ''
            ? 'No'
            : {
                value:
                  data.body.content.ApplicationDetails.Customer
                    .ExistingCustomer,
                disabled: true,
              },
          Validators.required,
        ],
        address: [
          data.body.content.ApplicationDetails.Customer.FullAddress ===
          undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.FullAddress,
                disabled: true,
              },
          Validators.required,
        ],
        incorporationAddress: [
          data.body.content.ApplicationDetails.Customer.PrimaryAddress ===
          undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.PrimaryAddress,
                disabled: true,
              },
          Validators.required,
        ],
        title: [
          data.body.content.ApplicationDetails.Customer.Contacts.NamePrefix ===
          undefined
            ? 'mr.'
            : data.body.content.ApplicationDetails.Customer.Contacts
                .NamePrefix === null
            ? 'mr.'
            : data.body.content.ApplicationDetails.Customer.Contacts
                .NamePrefix === ''
            ? 'mr.'
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.Contacts
                    .NamePrefix,
                disabled: true,
              },
          Validators.required,
        ],
        contactName: [
          data.body.content.ApplicationDetails.Customer.Contacts.pyFullName ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Contacts
                .pyFullName === null
            ? ''
            : data.body.content.ApplicationDetails.Customer.Contacts
                .pyFullName === ''
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.Contacts
                    .pyFullName,
                disabled: true,
              },
          [Validators.required, Validators.pattern('^[a-zA-Z]+$')],
        ],
        contactNumber: [
          data.body.content.ApplicationDetails.Customer.Contacts
            .pyPhoneNumber === undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.Contacts
                    .pyPhoneNumber,
                disabled: true,
              },
          [
            Validators.required,
            Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$'),
          ],
        ],
        emailAddress: [
          data.body.content.ApplicationDetails.Customer.Contacts.pyEmail1 ===
          undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.Contacts
                    .pyEmail1,
                disabled: true,
              },
          [Validators.required, Validators.email],
        ],
      });
    } else {
      this.profileFormGroup.patchValue({
        companyName:
          data.body.content.ApplicationDetails.Customer.FullName === undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.FullName,
        isExistingCustomer:
          data.body.content.ApplicationDetails.Customer.ExistingCustomer ===
          undefined
            ? 'No'
            : data.body.content.ApplicationDetails.Customer.ExistingCustomer,
        address:
          data.body.content.ApplicationDetails.Customer.FullAddress ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.FullAddress,
        incorporationAddress:
          data.body.content.ApplicationDetails.Customer.PrimaryAddress ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.PrimaryAddress,
        title:
          data.body.content.ApplicationDetails.Customer.Contacts.NamePrefix ===
          undefined
            ? 'mr.'
            : data.body.content.ApplicationDetails.Customer.Contacts.NamePrefix,
        contactName:
          data.body.content.ApplicationDetails.Customer.Contacts.pyFullName ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Contacts.pyFullName,
        contactNumber:
          data.body.content.ApplicationDetails.Customer.Contacts
            .pyPhoneNumber === undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Contacts
                .pyPhoneNumber,
        emailAddress:
          data.body.content.ApplicationDetails.Customer.Contacts.pyEmail1 ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Contacts.pyEmail1,
      });
    }
  }

  // convenience getter for easy access to form fields
  get profileControlName() {
    return this.profileFormGroup.controls;
  }

  profileSubmitData(num: number) {
    this.profileSubmitted = true;

    // stop here if form is invalid
    if (this.profileFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.updataFormData(
          this.profileFormGroup.value,
          this.companyFormGroup.value,
          this.loanFormGroup.value,
          this.calculatorFormGroup.value
        );

        this.loanService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            console.log(updata.headers);
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getLoanDetailsByCaseID();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              // this.router.navigateByUrl('/consumer/dashboard');
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.loanService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }

  //=================== end profile ================================

  //=================== start company ================================

  companyDetailsFormValidation(data: any) {
    let ProfitAndLoss =
      data.body.content.ApplicationDetails.Customer.ProfitAndLoss === undefined
        ? ''
        : data.body.content.ApplicationDetails.Customer.ProfitAndLoss;
    let BalanceSheetAssets =
      data.body.content.ApplicationDetails.Customer.BalanceSheetAssets ===
      undefined
        ? ''
        : data.body.content.ApplicationDetails.Customer.BalanceSheetAssets;
    let BalanceSheetLiabilities =
      data.body.content.ApplicationDetails.Customer.BalanceSheetLiabilities ===
      undefined
        ? ''
        : data.body.content.ApplicationDetails.Customer.BalanceSheetLiabilities;

    const netIncomeformArray = new FormArray([]);
    const assetsformArray = new FormArray([]);
    const liabilityformArray = new FormArray([]);

    //================profit and loss===========
    if (ProfitAndLoss.length === 0) {
      return;
    } else {
      ProfitAndLoss.forEach((x: any) => {
        netIncomeformArray.push(
          this._formBuilder.group({
            Income:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Income, disabled: true }]
                : [{ value: x.Income, disabled: false }],
            Interest:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Interest, disabled: true }]
                : [{ value: x.Interest, disabled: false }],
            Taxes:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Taxes, disabled: true }]
                : [{ value: x.Taxes, disabled: false }],
            NetResult:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.NetResult, disabled: true }]
                : [{ value: x.NetResult, disabled: false }],
            Period:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Period, disabled: true }]
                : [{ value: x.Period, disabled: false }],
          })
        );
      });
    }

    //========================== assets ===========================
    if (BalanceSheetAssets.length === 0) {
      return;
    } else {
      BalanceSheetAssets.forEach((x: any) => {
        assetsformArray.push(
          this._formBuilder.group({
            Inventory:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Inventory, disabled: true }]
                : [{ value: x.Inventory, disabled: false }],
            LiquidAssets:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.LiquidAssets, disabled: true }]
                : [{ value: x.LiquidAssets, disabled: false }],
            Machinery:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Machinery, disabled: true }]
                : [{ value: x.Machinery, disabled: false }],
            Goodwill:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Goodwill, disabled: true }]
                : [{ value: x.Goodwill, disabled: false }],
            Period:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Period, disabled: true }]
                : [{ value: x.Period, disabled: false }],
            RealEstate:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.RealEstate, disabled: true }]
                : [{ value: x.RealEstate, disabled: false }],
            Stock:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Stock, disabled: true }]
                : [{ value: x.Stock, disabled: false }],
            TotalAssets:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.TotalAssets, disabled: true }]
                : [{ value: x.TotalAssets, disabled: false }],
          })
        );
      });
    }

    //================liabilities=============================
    if (BalanceSheetLiabilities.length === 0) {
      return;
    } else {
      BalanceSheetLiabilities.forEach((x: any) => {
        liabilityformArray.push(
          this._formBuilder.group({
            Equity:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Equity, disabled: true }]
                : [{ value: x.Equity, disabled: false }],
            Period:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.Period, disabled: true }]
                : [{ value: x.Period, disabled: false }],
            ShortTermDebt:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.ShortTermDebt, disabled: true }]
                : [{ value: x.ShortTermDebt, disabled: false }],
            LongTermDebt:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.LongTermDebt, disabled: true }]
                : [{ value: x.LongTermDebt, disabled: false }],
            TotalLiabilities:
              data.body.content.pxCurrentStage != 'PRIM2'
                ? [{ value: x.TotalLiabilities, disabled: true }]
                : [{ value: x.TotalLiabilities, disabled: false }],
          })
        );
      });
    }

    if (data.body.content.pxCurrentStage != 'PRIM2') {
      this.companyFormGroup = this._formBuilder.group({
        companyType: [
          data.body.content.ApplicationDetails.Customer.CompanyType ===
          undefined
            ? 'Public'
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.CompanyType,
                disabled: true,
              },
        ],
        buisnessType: [
          data.body.content.ApplicationDetails.Customer.TypeOfBusiness ===
          undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer.TypeOfBusiness,
                disabled: true,
              },
        ],
        sector: [
          data.body.content.ApplicationDetails.Customer.Sector === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.Customer.Sector,
                disabled: true,
              },
        ],
        employeeNumber:
          data.body.content.ApplicationDetails.Customer.NumberOfEmployees ===
          undefined
            ? ''
            : {
                value:
                  data.body.content.ApplicationDetails.Customer
                    .NumberOfEmployees,
                disabled: true,
              },
        location:
          data.body.content.ApplicationDetails.Customer.Location === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.Customer.Location,
                disabled: true,
              },
        profitloses: netIncomeformArray,
        assets: assetsformArray,
        liabilities: liabilityformArray,
      });
    } else {
      this.companyFormGroup.patchValue({
        companyType:
          data.body.content.ApplicationDetails.Customer.CompanyType ===
          undefined
            ? 'Public'
            : data.body.content.ApplicationDetails.Customer.CompanyType,
        buisnessType:
          data.body.content.ApplicationDetails.Customer.TypeOfBusiness ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.TypeOfBusiness,
        sector:
          data.body.content.ApplicationDetails.Customer.Sector === undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Sector,
        employeeNumber:
          data.body.content.ApplicationDetails.Customer.NumberOfEmployees ===
          undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.NumberOfEmployees,
        location:
          data.body.content.ApplicationDetails.Customer.Location === undefined
            ? ''
            : data.body.content.ApplicationDetails.Customer.Location,
      });

      this.companyFormGroup.setControl('profitloses', netIncomeformArray);
      this.companyFormGroup.setControl('assets', assetsformArray);
      this.companyFormGroup.setControl('liabilities', liabilityformArray);
    }
  }
  // convenience getter for easy access to form fields
  get companyControlName() {
    return this.companyFormGroup.controls;
  }

  profitloses(): FormArray {
    return this.companyFormGroup.get('profitloses') as FormArray;
  }
  assets(): FormArray {
    return this.companyFormGroup.get('assets') as FormArray;
  }
  liabilities(): FormArray {
    return this.companyFormGroup.get('liabilities') as FormArray;
  }

  newQuantity(): FormGroup {
    return this._formBuilder.group({
      Income: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Interest: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Taxes: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      NetResult: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ], //net income
      Period: ['', Validators.required], // year
    });
  }
  newAsset(): FormGroup {
    return this._formBuilder.group({
      Inventory: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      LiquidAssets: '11241',
      Machinery: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Goodwill: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Period: ['', Validators.required],
      RealEstate: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Stock: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      TotalAssets: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
    });
  }

  newLiability(): FormGroup {
    return this._formBuilder.group({
      Equity: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      Period: ['', Validators.required],
      ShortTermDebt: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      LongTermDebt: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      TotalLiabilities: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
    });
  }

  addQuantity(dat: any) {
    if (dat === 'profitandlos') {
      this.profitloses().push(this.newQuantity());
    } else if (dat === 'assets') {
      this.assets().push(this.newAsset());
    } else {
      this.liabilities().push(this.newLiability());
    }
  }

  removeQuantity(i: number, da: any) {
    if (da === 'profitandlos') {
      this.profitloses().removeAt(i);
    } else if (da === 'assets') {
      this.assets().removeAt(i);
    } else {
      this.liabilities().removeAt(i);
    }
  }

  companySubmitData(num: number) {
    this.companySubmitted = true;
    if (this.companyFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.updataFormData(
          this.profileFormGroup.value,
          this.companyFormGroup.value,
          this.loanFormGroup.value,
          this.calculatorFormGroup.value
        );

        this.loanService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getLoanDetailsByCaseID();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              // this.router.navigateByUrl('/consumer/dashboard');
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );
      }

      // this.loanService.getCaseById(this.caseId).subscribe(
      //   (resp) => {
      //     if (resp.status === 200) {
      //       localStorage.removeItem('etag');
      //       localStorage.setItem('etag', resp.headers.get('etag'));

      //     }
      //   },
      //   (error) => {
      //     this.notificationService.showError(
      //       `${error.message}`,
      //       'Server error occurs'
      //     );
      //   }
      // );
    }
  }
  //=================== end company ================================

  //=================== start loan================================

  transformAmount(element: any, da: any) {
    if (da === 'la') {
      this.formattedAmount_la = element.target.value;
    } else {
      this.formattedAmount_cv = element.target.value;
    }
  }

  loanDetailsFormValidation(data: any) {
    if (data.body.content.pxCurrentStage != 'PRIM2') {
      this.loanFormGroup.controls['filename'].disable();
      this.loanFormGroup = this._formBuilder.group({
        loanAmount:
          data.body.content.ApplicationDetails.RequestedLoanAmount === undefined
            ? 0
            : {
                value: parseFloat(
                  data.body.content.ApplicationDetails.RequestedLoanAmount
                ),
                disabled: true,
              },
        loanType:
          data.body.content.ApplicationDetails.LoanType === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.LoanType,
                disabled: true,
              },
        loanPurpose:
          data.body.content.ApplicationDetails.LoanPurpose === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.LoanPurpose,
                disabled: true,
              },
        loanTenure:
          data.body.content.ApplicationDetails.LoanTenure === undefined
            ? 0
            : {
                value: parseInt(
                  data.body.content.ApplicationDetails.LoanTenure
                ),
                disabled: true,
              },
        isLoanCollaterized:
          data.body.content.ApplicationDetails.LoanCollateralized === undefined
            ? 'No'
            : {
                value: data.body.content.ApplicationDetails.LoanCollateralized,
                disabled: true,
              },
        collateralType:
          data.body.content.ApplicationDetails.CollateralType === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.CollateralType,
                disabled: true,
              },
        checkLoanPeriod:
          data.body.content.ApplicationDetails.LoanPeriodExceeding === 'true'
            ? true
            : false,
        collateralValue:
          data.body.content.ApplicationDetails.CollateralValue === undefined
            ? 0
            : {
                value: parseFloat(
                  data.body.content.ApplicationDetails.CollateralValue
                ),
                disabled: true,
              },
        filename: { value: '', disabled: true },
      });
    } else {
      this.loanFormGroup.patchValue({
        loanAmount:
          data.body.content.ApplicationDetails.RequestedLoanAmount === undefined
            ? 0
            : parseFloat(
                data.body.content.ApplicationDetails.RequestedLoanAmount
              ),
        loanType:
          data.body.content.ApplicationDetails.LoanType === undefined
            ? ''
            : data.body.content.ApplicationDetails.LoanType,
        loanPurpose:
          data.body.content.ApplicationDetails.LoanPurpose === undefined
            ? ''
            : data.body.content.ApplicationDetails.LoanPurpose,
        loanTenure:
          data.body.content.ApplicationDetails.LoanTenure === undefined
            ? 0
            : parseInt(data.body.content.ApplicationDetails.LoanTenure),
        isLoanCollaterized:
          data.body.content.ApplicationDetails.LoanCollateralized === undefined
            ? 'No'
            : data.body.content.ApplicationDetails.LoanCollateralized,
        collateralType:
          data.body.content.ApplicationDetails.CollateralType === undefined
            ? ''
            : data.body.content.ApplicationDetails.CollateralType,
        checkLoanPeriod:
          data.body.content.ApplicationDetails.LoanPeriodExceeding === 'true'
            ? true
            : false,
        collateralValue:
          data.body.content.ApplicationDetails.CollateralValue === undefined
            ? 0
            : parseFloat(data.body.content.ApplicationDetails.CollateralValue),
        filename: [''],
      });
    }
  }
  // convenience getter for easy access to form fields
  get loanControlName() {
    return this.loanFormGroup.controls;
  }

  onChange(event: any) {
    if (event.target.files.length > 0) {
      this.fileName = event.target.files[0].name;
    }
  }

  selectItem(e: any) {
    if (e === true) {
      this.showBankExcutiveHelp = true;
    } else {
      this.showBankExcutiveHelp = false;
    }
  }

  loanSubmitData(num: number) {
    this.loanSubmitted = true;

    // stop here if form is invalid
    if (this.loanFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.updataFormData(
          this.profileFormGroup.value,
          this.companyFormGroup.value,
          this.loanFormGroup.value,
          this.calculatorFormGroup.value
        );

        this.loanService.getCaseById(this.caseId).subscribe(
          (resp) => {
            if (resp.status === 200) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', resp.headers.get('etag'));
              this.loanService
                .updateCaseById(this.caseId, returnReqData)
                .subscribe(
                  (updata) => {
                    if (updata.status === 204) {
                      // this.getLoanDetailsByCaseID();
                      localStorage.removeItem('etag');
                      localStorage.setItem('etag', updata.headers.get('etag'));

                      let str = this.route.snapshot.params['caseId'];
                      let caseName = str.substring(str.lastIndexOf(' ') + 1);

                      let calcuData = this.calculatorDataFormat(
                        this.profileFormGroup.value,
                        this.loanFormGroup.value,
                        this.companyFormGroup.value,
                        this.calculatorFormGroup.value
                      );
                      this.loanService
                        .loanCalculatorDialet(calcuData)
                        .subscribe(
                          (data) => {
                            if (data.status === 200) {
                              this.calculatorFormGroup.patchValue({
                                Decision:
                                  data.body.CreditAssessmentResultList[0]
                                    .Decision,
                                DecisionCode:
                                  data.body.CreditAssessmentResultList[0]
                                    .DecisionCode,
                                Feedback:
                                  data.body.CreditAssessmentResultList[0]
                                    .Feedback,
                                Score:
                                  data.body.CreditAssessmentResultList[0].Score,

                                alt_Alternative: 'Collateral Value',
                                alt_AlternativeValue: parseInt(
                                  resp.body.content.ApplicationDetails
                                    .CollateralValue
                                ),

                                alt1_Alternative: 'Requested Loan Amount',
                                alt1_AlternativeValue: parseInt(
                                  resp.body.content.ApplicationDetails
                                    .RequestedLoanAmount
                                ),
                              });

                              let reReqData = this.updataFormData(
                                this.profileFormGroup.value,
                                this.companyFormGroup.value,
                                this.loanFormGroup.value,
                                this.calculatorFormGroup.value
                              );

                              this.loanService
                                .updateCaseById(this.caseId, reReqData)
                                .subscribe((data) => {
                                  if (data.status === 204) {
                                    this.getLoanDetailsByCaseID();
                                    this.step = num;
                                    setTimeout(
                                      () =>
                                        (this.stepper.selectedIndex = num - 1)
                                    );
                                  }
                                });

                              // this.loanService
                              //   .getCaseById(this.caseId)
                              //   .subscribe((resp) => {
                              //     if (resp.status === 200) {
                              //       localStorage.removeItem('etag');
                              //       localStorage.setItem(
                              //         'etag',
                              //         resp.headers.get('etag')
                              //       );

                              //     }
                              //   });
                            }
                          },
                          (error) => {
                            console.log(error);
                          }
                        );

                      // this.notificationService.showSuccess(
                      //   'Case updated successfully',
                      //   `${caseName}`
                      // );
                      // this.router.navigateByUrl('/consumer/dashboard');
                    }
                  },
                  (error) => {
                    let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
                    this.notificationService.showError(
                      `${errorMessage}`,
                      'Server error occurs'
                    );
                  }
                );
            }
          },
          (error) => {
            this.notificationService.showError(
              `${error.message}`,
              'Server error occurs'
            );
          }
        );
      }
    }
  }

  //=================== start calculator================================

  //================ calculator value format===============================
  onBlurClick(event: any, dat: any) {
    if (dat == 'c') {
      let calcuData = this.calculatorDataFormatCalculatorPage(
        this.profileFormGroup.value,
        this.loanFormGroup.value,
        this.companyFormGroup.value,
        this.calculatorFormGroup.value
      );

      /** spinner starts on init */
      this.spinner.show();

      this.loanService.loanCalculatorDialet(calcuData).subscribe(
        (data) => {
          if (data.status === 200) {
            this.calculatorFormGroup.patchValue({
              Decision: data.body.CreditAssessmentResultList[0].Decision,
              DecisionCode:
                data.body.CreditAssessmentResultList[0].DecisionCode,
              Feedback: data.body.CreditAssessmentResultList[0].Feedback,
              Score: data.body.CreditAssessmentResultList[0].Score,
            });
            let dataKeyPress = this.updataFormDataAfterKeyPress(
              this.profileFormGroup.value,
              this.companyFormGroup.value,
              this.loanFormGroup.value,
              this.calculatorFormGroup.value
            );

            this.loanService
              .updateCaseById(this.caseId, dataKeyPress)
              .subscribe((data) => {
                if (data.status === 204) {
                  localStorage.removeItem('etag');
                  localStorage.setItem('etag', data.headers.get('etag'));
                  this.getLoanDetailsByCaseID();
                  setTimeout(() => {
                    /** spinner ends after 5 seconds */
                    this.spinner.hide();
                  }, 1000);
                }
              });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else if (dat == 'r') {
      let calcuData = this.calculatorDataFormatCalculatorPage(
        this.profileFormGroup.value,
        this.loanFormGroup.value,
        this.companyFormGroup.value,
        this.calculatorFormGroup.value
      );
      /** spinner starts on init */
      this.spinner.show();

      this.loanService.loanCalculatorDialet(calcuData).subscribe(
        (data) => {
          if (data.status === 200) {
            this.calculatorFormGroup.patchValue({
              Decision: data.body.CreditAssessmentResultList[0].Decision,
              DecisionCode:
                data.body.CreditAssessmentResultList[0].DecisionCode,
              Feedback: data.body.CreditAssessmentResultList[0].Feedback,
              Score: data.body.CreditAssessmentResultList[0].Score,
            });
            let dataKeyPress = this.updataFormDataAfterKeyPress(
              this.profileFormGroup.value,
              this.companyFormGroup.value,
              this.loanFormGroup.value,
              this.calculatorFormGroup.value
            );

            this.loanService
              .updateCaseById(this.caseId, dataKeyPress)
              .subscribe((data) => {
                if (data.status === 204) {
                  localStorage.removeItem('etag');
                  localStorage.setItem('etag', data.headers.get('etag'));
                  this.getLoanDetailsByCaseID();
                  setTimeout(() => {
                    /** spinner ends after 5 seconds */
                    this.spinner.hide();
                  }, 1000);
                }
              });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.spinner.show();
      let calcuData = this.calculatorDataFormatCalculatorPage(
        this.profileFormGroup.value,
        this.loanFormGroup.value,
        this.companyFormGroup.value,
        this.calculatorFormGroup.value
      );

      this.loanService.loanCalculatorDialet(calcuData).subscribe(
        (data) => {
          if (data.status === 200) {
            this.calculatorFormGroup.patchValue({
              Decision: data.body.CreditAssessmentResultList[0].Decision,
              DecisionCode:
                data.body.CreditAssessmentResultList[0].DecisionCode,
              Feedback: data.body.CreditAssessmentResultList[0].Feedback,
              Score: data.body.CreditAssessmentResultList[0].Score,
            });
            let dataKeyPress = this.updataFormDataAfterKeyPress(
              this.profileFormGroup.value,
              this.companyFormGroup.value,
              this.loanFormGroup.value,
              this.calculatorFormGroup.value
            );

            this.loanService
              .updateCaseById(this.caseId, dataKeyPress)
              .subscribe((data) => {
                if (data.status === 204) {
                  localStorage.removeItem('etag');
                  localStorage.setItem('etag', data.headers.get('etag'));
                  this.getLoanDetailsByCaseID();
                  setTimeout(() => {
                    /** spinner ends after 5 seconds */
                    this.spinner.hide();
                  }, 1000);
                }
              });

            // this.loanService.getCaseById(this.caseId).subscribe((resp) => {
            //   if (resp.status === 200) {
            //     localStorage.removeItem('etag');
            //     localStorage.setItem('etag', resp.headers.get('etag'));

            //   }
            // });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  calculatorDetailsFormValidation(data: any) {
    if (data.body.content.pxCurrentStage != 'PRIM2') {
      this.calculatorFormGroup = this._formBuilder.group({
        loanPurpose:
          data.body.content.ApplicationDetails.LoanPurpose === undefined
            ? ''
            : {
                value: data.body.content.ApplicationDetails.LoanPurpose,
                disabled: true,
              },
        requestedLoanAmount:
          data.body.content.ApplicationDetails.RequestedLoanAmount === undefined
            ? 0
            : {
                value: parseFloat(
                  data.body.content.ApplicationDetails.RequestedLoanAmount
                ),
                disabled: true,
              },
        collateralValue:
          data.body.content.ApplicationDetails.CollateralValue === undefined
            ? 0
            : {
                value: parseFloat(
                  data.body.content.ApplicationDetails.CollateralValue
                ),
                disabled: true,
              },

        Decision: '',
        DecisionCode: '',
        Feedback: '',
        Score: '',
        alt_Alternative: '',
        alt_AlternativeValue: '',
        alt_Decision: '',
        alt_DecisionCode: '',
        alt_Feedback: '',
        alt_NumberOfAlternatives: '',
        alt1_Alternative: '',
        alt1_AlternativeValue: '',
        alt1_Decision: '',
        alt1_DecisionCode: '',
        alt1_Feedback: '',
        alt1_NumberOfAlternatives: '',
      });
    } else {
      this.calculatorFormGroup.patchValue({
        requestedLoanAmount:
          data.body.content.ApplicationDetails.RequestedLoanAmount === undefined
            ? 0
            : parseFloat(
                data.body.content.ApplicationDetails.RequestedLoanAmount
              ),
        collateralValue:
          data.body.content.ApplicationDetails.CollateralValue === undefined
            ? 0
            : parseFloat(data.body.content.ApplicationDetails.CollateralValue),
        loanPurpose:
          data.body.content.ApplicationDetails.LoanPurpose === undefined
            ? ''
            : data.body.content.ApplicationDetails.LoanPurpose,

        Decision:
          data.body.content.ApplicationDetails.CreditAssessmentResultList ===
          undefined
            ? 'Declined but alternatives available'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Decision === undefined
            ? 'Declined but alternatives available'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Decision,
        DecisionCode:
          data.body.content.ApplicationDetails.CreditAssessmentResultList ===
          undefined
            ? 'DA'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .DecisionCode === undefined
            ? 'DA'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .DecisionCode,
        Feedback:
          data.body.content.ApplicationDetails.CreditAssessmentResultList ===
          undefined
            ? "Let's have a closer look at the income figures"
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Feedback === undefined
            ? "Let's have a closer look at the income figures"
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Feedback,
        Score:
          data.body.content.ApplicationDetails.CreditAssessmentResultList ===
          undefined
            ? '435.0'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Score === undefined
            ? '435.0'
            : data.body.content.ApplicationDetails.CreditAssessmentResultList[0]
                .Score,

        alt_Alternative:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Collateral Value'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Alternative === undefined
            ? 'Collateral Value'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Alternative,
        alt_AlternativeValue:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? '1870000.0'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].AlternativeValue === undefined
            ? '1870000.0'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].AlternativeValue,
        alt_Decision:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Approved'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Decision === undefined
            ? 'Approved'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Decision,
        alt_DecisionCode:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'A'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].DecisionCode === undefined
            ? 'A'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].DecisionCode,
        alt_Feedback:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Alternative(s) found'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Feedback === undefined
            ? 'Alternative(s) found'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].Feedback,
        alt_NumberOfAlternatives:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? '20'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].NumberOfAlternatives ===
              undefined
            ? '20'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[0].NumberOfAlternatives,

        alt1_Alternative:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Requested Loan Amount'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Alternative === undefined
            ? 'Requested Loan Amount'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Alternative,
        alt1_AlternativeValue:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? '1050000.0'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].AlternativeValue === undefined
            ? '1050000.0'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].AlternativeValue,
        alt1_Decision:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Approved'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Decision === undefined
            ? 'Approved'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Decision,
        alt1_DecisionCode:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'A'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].DecisionCode === undefined
            ? 'A'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].DecisionCode,
        alt1_Feedback:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? 'Alternative(s) found'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Feedback === undefined
            ? 'Alternative(s) found'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].Feedback,
        alt1_NumberOfAlternatives:
          data.body.content.ApplicationDetails.CreditAlternativeResultList ===
          undefined
            ? '10'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].NumberOfAlternatives ===
              undefined
            ? '10'
            : data.body.content.ApplicationDetails
                .CreditAlternativeResultList[1].NumberOfAlternatives,
      });
    }
  }

  onBlurEvent(event: any) {
    this.needleValue = event.target.value;
    this.bottomLabel = event.target.value;
  }

  calculatorSubmitData() {
    this.calculatorSubmitted = true;
    if (this.calculatorFormGroup.invalid) {
      return;
    } else {
      let returnReqData = this.updataFormData(
        this.profileFormGroup.value,
        this.companyFormGroup.value,
        this.loanFormGroup.value,
        this.calculatorFormGroup.value
      );

      this.loanService.getCaseById(this.caseId).subscribe(
        (resp) => {
          if (resp.status === 200) {
            localStorage.removeItem('etag');
            localStorage.setItem('etag', resp.headers.get('etag'));
            this.loanService
              .updateCaseById(this.caseId, returnReqData)
              .subscribe(
                (updata) => {
                  let changeSta = this.changeStateData();

                  this.loanService
                    .getCaseById(this.caseId)
                    .subscribe((respp) => {
                      if (resp.status === 200) {
                        localStorage.removeItem('etag');
                        localStorage.setItem('etag', respp.headers.get('etag'));
                        this.loanService
                          .updateChangeStateById(this.caseId, changeSta)
                          .subscribe((data) => {
                            if (data.status === 204) {
                              let str = this.route.snapshot.params['caseId'];
                              let caseName = str.substring(
                                str.lastIndexOf(' ') + 1
                              );
                              this.notificationService.showSuccess(
                                'Case updated successfully',
                                `${caseName}`
                              );
                              this.router.navigateByUrl(
                                `/home/confirm/${caseName}`
                              );
                            }
                          });
                      }
                    });
                },
                (error) => {
                  let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
                  this.notificationService.showError(
                    `${errorMessage}`,
                    'Server error occurs'
                  );
                }
              );
          }
        },
        (error) => {
          console.log('get_api_call_before_update_error', error);
          this.notificationService.showError(
            `${error.errors[0].message}`,
            'Server error occurs'
          );
        }
      );
    }
  }

  //===================== JSON Data Formatting=========================
  updataFormData(
    profileData: any,
    companyData: any,
    loanData: any,
    calculatorData: any
  ) {
    //===============profile data==================
    let p_companyName = profileData.companyName;
    let p_isExistingCustomer = profileData.isExistingCustomer;
    let p_address = profileData.address;
    let p_incorporationAddress = profileData.incorporationAddress;
    let p_title = profileData.title;
    let p_contactName = profileData.contactName;
    let p_contactNumber = profileData.contactNumber;
    let p_emailAddress = profileData.emailAddress;

    //================company data=========================
    let c_companyType = companyData.companyType;
    let c_buisnessType = companyData.buisnessType;
    let c_sector = companyData.sector;
    let c_employeeNumber = companyData.employeeNumber;
    let c_location = companyData.location;
    let c_profitloses = companyData.profitloses;
    let c_assets = companyData.assets;
    let c_liabilities = companyData.liabilities;

    //================company data=========================
    let l_loanAmount = loanData.loanAmount;
    let l_loanType = loanData.loanType;
    let l_loanPurpose = loanData.loanPurpose;
    let l_loanTenure = loanData.loanTenure;
    let l_isLoanCollaterized = loanData.isLoanCollaterized;
    let l_collateralType = loanData.collateralType;
    let l_checkLoanPeriod = loanData.checkLoanPeriod;
    let l_collateralValue = loanData.collateralValue;
    let l_filename = loanData.filename;

    //==================== calculator data==========================
    let Decision = calculatorData.Decision;
    let DecisionCode = calculatorData.DecisionCode;
    let Feedback = calculatorData.Feedback;
    let Score = calculatorData.Score;

    let alt_Alternative1 = calculatorData.alt_Alternative;
    let alt_AlternativeValue1 = calculatorData.alt_AlternativeValue;
    let alt_Decision1 = calculatorData.alt_Decision;
    let alt_DecisionCode1 = calculatorData.alt_DecisionCode;
    let alt_Feedback1 = calculatorData.alt_Feedback;
    let alt_NumberOfAlternatives1 = calculatorData.alt_NumberOfAlternatives;

    let alt1_Alternative1 = calculatorData.alt1_Alternative;
    let alt1_AlternativeValue1 = calculatorData.alt1_AlternativeValue;
    let alt1_Decision1 = calculatorData.alt1_Decision;
    let alt1_DecisionCode1 = calculatorData.alt1_DecisionCode;
    let alt1_Feedback1 = calculatorData.alt1_Feedback;
    let alt1_NumberOfAlternatives1 = calculatorData.alt1_NumberOfAlternatives;

    let updateformData = {
      content: {
        ApplicationDetails: {
          CollateralType: l_collateralType, //loan CollateralType
          CollateralValue: l_collateralValue, //loan CollateralValue
          LoanCollateralized: l_isLoanCollaterized, //Will this loan be collateralized?
          LoanPeriodExceeding: l_checkLoanPeriod,
          LoanPurpose: l_loanPurpose, //loan purpose
          LoanType: l_loanType, //loan type
          RequestedLoanAmount: l_loanAmount, //loan amount of loan tab
          LoanTenure: l_loanTenure, //LoanTenure
          CreditAlternativeResultList: [
            {
              Alternative: alt_Alternative1,
              AlternativeValue: l_collateralValue,
              ChangePercentage: '0.1000',
              Decision: Decision, //"Approved",
              DecisionCode: DecisionCode, //"A",
              Feedback: Feedback, //"Alternative(s) found",
              NumberOfAlternatives: '20',
              Score: Score, //"775.0"
            },
            {
              Alternative: alt1_Alternative1,
              AlternativeValue: l_loanAmount,
              ChangePercentage: '-0.3000',
              Decision: Decision, //"Approved",
              DecisionCode: DecisionCode, //"A",
              Feedback: Feedback, //"Alternative(s) found",
              NumberOfAlternatives: '10',
              Score: Score, //"675.0"
            },
          ],
          CreditAssessmentResult: {
            Decision: Decision,
            DecisionCode: DecisionCode,
            Feedback: Feedback,
            Score: Score,
          },
          CreditAssessmentResultList: [
            {
              Decision: Decision,
              DecisionCode: DecisionCode,
              Feedback: Feedback,
              Score: Score,
            },
          ],
          Customer: {
            CompanyType: c_companyType, //company type
            ExistingCustomer: p_isExistingCustomer, //profile ExistingCustomer
            FullAddress: p_address, //profile address
            FullName: p_companyName, //profile company name
            Location: c_location, //company type oy Location
            NumberOfEmployees: c_employeeNumber, //company number of employee
            PrimaryAddress: p_incorporationAddress, //profile leagal address
            Sector: c_sector, //company type oy Sector
            TypeOfBusiness: c_buisnessType, //company type oy buisness

            BalanceSheetAssets: c_assets,

            BalanceSheetLiabilities: c_liabilities,

            Contacts: {
              NamePrefix: p_title, //profile title
              pyEmail1: p_emailAddress, //profile email address
              pyFullName: p_contactName, //profile contact Name
              pyPhoneNumber: p_contactNumber, //profile contact number
            },
            ProfitAndLoss: c_profitloses,
          },
        },
      },
    };

    return updateformData;
  }

  //===================== JSON Data Formatting=========================
  updataFormDataAfterKeyPress(
    profileData: any,
    companyData: any,
    loanData: any,
    calculatorData: any
  ) {
    //===============profile data==================
    let p_companyName = profileData.companyName;
    let p_isExistingCustomer = profileData.isExistingCustomer;
    let p_address = profileData.address;
    let p_incorporationAddress = profileData.incorporationAddress;
    let p_title = profileData.title;
    let p_contactName = profileData.contactName;
    let p_contactNumber = profileData.contactNumber;
    let p_emailAddress = profileData.emailAddress;

    //================company data=========================
    let c_companyType = companyData.companyType;
    let c_buisnessType = companyData.buisnessType;
    let c_sector = companyData.sector;
    let c_employeeNumber = companyData.employeeNumber;
    let c_location = companyData.location;
    let c_profitloses = companyData.profitloses;
    let c_assets = companyData.assets;
    let c_liabilities = companyData.liabilities;

    //================company data=========================
    let l_loanAmount = loanData.loanAmount;
    let l_loanType = loanData.loanType;
    let l_loanPurpose = loanData.loanPurpose;
    let l_loanTenure = loanData.loanTenure;
    let l_isLoanCollaterized = loanData.isLoanCollaterized;
    let l_collateralType = loanData.collateralType;
    let l_checkLoanPeriod = loanData.checkLoanPeriod;
    let l_collateralValue = loanData.collateralValue;
    let l_filename = loanData.filename;

    //==================== calculator data==========================
    let Decision = calculatorData.Decision;
    let DecisionCode = calculatorData.DecisionCode;
    let Feedback = calculatorData.Feedback;
    let Score = calculatorData.Score;

    let cal_loanPurpose = calculatorData.loanPurpose;
    let cal_requestedLoanAmount = calculatorData.requestedLoanAmount;
    let cal_collateralValue = calculatorData.collateralValue;

    let alt_Alternative1 = calculatorData.alt_Alternative;
    let alt_AlternativeValue1 = calculatorData.alt_AlternativeValue;
    let alt_Decision1 = calculatorData.alt_Decision;
    let alt_DecisionCode1 = calculatorData.alt_DecisionCode;
    let alt_Feedback1 = calculatorData.alt_Feedback;
    let alt_NumberOfAlternatives1 = calculatorData.alt_NumberOfAlternatives;

    let alt1_Alternative1 = calculatorData.alt1_Alternative;
    let alt1_AlternativeValue1 = calculatorData.alt1_AlternativeValue;
    let alt1_Decision1 = calculatorData.alt1_Decision;
    let alt1_DecisionCode1 = calculatorData.alt1_DecisionCode;
    let alt1_Feedback1 = calculatorData.alt1_Feedback;
    let alt1_NumberOfAlternatives1 = calculatorData.alt1_NumberOfAlternatives;

    let updateformDataA = {
      content: {
        ApplicationDetails: {
          CollateralType: l_collateralType, //loan CollateralType
          CollateralValue: cal_collateralValue, //loan CollateralValue
          LoanCollateralized: l_isLoanCollaterized, //Will this loan be collateralized?
          LoanPeriodExceeding: l_checkLoanPeriod,
          LoanPurpose: cal_loanPurpose, //loan purpose
          LoanType: l_loanType, //loan type
          RequestedLoanAmount: cal_requestedLoanAmount, //loan amount of loan tab
          LoanTenure: l_loanTenure, //LoanTenure
          CreditAlternativeResultList: [
            {
              Alternative: alt_Alternative1,
              AlternativeValue: cal_collateralValue,
              ChangePercentage: '0.1000',
              Decision: Decision, //"Approved",
              DecisionCode: DecisionCode, //"A",
              Feedback: Feedback, //"Alternative(s) found",
              NumberOfAlternatives: '20',
              Score: Score, //"775.0"
            },
            {
              Alternative: alt1_Alternative1,
              AlternativeValue: cal_requestedLoanAmount,
              ChangePercentage: '-0.3000',
              Decision: Decision, //"Approved",
              DecisionCode: DecisionCode, //"A",
              Feedback: Feedback, //"Alternative(s) found",
              NumberOfAlternatives: '10',
              Score: Score, //"675.0"
            },
          ],
          CreditAssessmentResult: {
            Decision: Decision,
            DecisionCode: DecisionCode,
            Feedback: Feedback,
            Score: Score,
          },
          CreditAssessmentResultList: [
            {
              Decision: Decision,
              DecisionCode: DecisionCode,
              Feedback: Feedback,
              Score: Score,
            },
          ],
          Customer: {
            CompanyType: c_companyType, //company type
            ExistingCustomer: p_isExistingCustomer, //profile ExistingCustomer
            FullAddress: p_address, //profile address
            FullName: p_companyName, //profile company name
            Location: c_location, //company type oy Location
            NumberOfEmployees: c_employeeNumber, //company number of employee
            PrimaryAddress: p_incorporationAddress, //profile leagal address
            Sector: c_sector, //company type oy Sector
            TypeOfBusiness: c_buisnessType, //company type oy buisness

            BalanceSheetAssets: c_assets,

            BalanceSheetLiabilities: c_liabilities,

            Contacts: {
              NamePrefix: p_title, //profile title
              pyEmail1: p_emailAddress, //profile email address
              pyFullName: p_contactName, //profile contact Name
              pyPhoneNumber: p_contactNumber, //profile contact number
            },
            ProfitAndLoss: c_profitloses,
          },
        },
      },
    };

    return updateformDataA;
  }

  //======================= cal culat data format============================
  calculatorDataFormat(
    profileData: any,
    loanData: any,
    companyData: any,
    calculatorData: any
  ) {
    let colVal;
    let loanVal;
    if (
      typeof loanData.collateralValue === 'string' ||
      loanData.collateralValue instanceof String
    ) {
      colVal = parseInt(loanData.collateralValue);
    } else {
      colVal = loanData.collateralValue;
    }
    if (
      typeof loanData.loanAmount === 'string' ||
      loanData.loanAmount instanceof String
    ) {
      loanVal = parseInt(loanData.loanAmount);
    } else {
      loanVal = loanData.loanAmount;
    }

    let l_loanAmount = loanVal; //loanData.loanAmount
    let l_loanType = loanData.loanType;
    let l_isLoanCollaterized = loanData.isLoanCollaterized;
    let l_collateralType = loanData.collateralType;
    let l_checkLoanPeriod = loanData.checkLoanPeriod;
    let l_collateralValue = colVal; //loanData.collateralValue
    let l_loanPurpose = loanData.loanPurpose;
    //================company data=========================
    let c_profitloses = companyData.profitloses;
    let c_assets = companyData.assets;
    let c_liabilities = companyData.liabilities;

    //===============profile data==================
    let p_title = profileData.title;
    let p_contactName = profileData.contactName;
    let p_contactNumber = profileData.contactNumber;
    let p_emailAddress = profileData.emailAddress;

    let calData = {
      CollateralType: l_collateralType,
      CollateralValue: l_collateralValue,
      LoanCollateralized: l_isLoanCollaterized,
      LoanPeriodExceeding: l_checkLoanPeriod,
      LoanPurpose: l_loanPurpose,
      LoanType: l_loanType,
      RequestedLoanAmount: l_loanAmount,
      Customer: {
        BalanceSheetAssets: c_assets,
        BalanceSheetLiabilities: c_liabilities,
        Contacts: {
          NamePrefix: p_title,
          pyEmail1: p_emailAddress,
          pyFullName: p_contactName,
          pyPhoneNumber: p_contactNumber,
        },
        ProfitAndLoss: c_profitloses,
      },
    };
    return calData;
  }

  calculatorDataFormatCalculatorPage(
    profileData: any,
    loanData: any,
    companyData: any,
    calculatorData: any
  ) {
    let c_loanAmount = parseFloat(calculatorData.requestedLoanAmount);
    let l_loanType = loanData.loanType;
    let l_isLoanCollaterized = loanData.isLoanCollaterized;
    let l_collateralType = loanData.collateralType;
    let l_checkLoanPeriod = loanData.checkLoanPeriod;
    let c_collateralValue = parseFloat(calculatorData.collateralValue);
    let c_loanPurpose = calculatorData.loanPurpose;

    //================company data=========================
    let c_profitloses = companyData.profitloses;
    let c_assets = companyData.assets;
    let c_liabilities = companyData.liabilities;

    //===============profile data==================
    let p_title = profileData.title;
    let p_contactName = profileData.contactName;
    let p_contactNumber = profileData.contactNumber;
    let p_emailAddress = profileData.emailAddress;

    let calData = {
      CollateralType: l_collateralType,
      CollateralValue: c_collateralValue,
      LoanCollateralized: l_isLoanCollaterized,
      LoanPeriodExceeding: l_checkLoanPeriod,
      LoanPurpose: c_loanPurpose,
      LoanType: l_loanType,
      RequestedLoanAmount: c_loanAmount,
      Customer: {
        BalanceSheetAssets: c_assets,
        BalanceSheetLiabilities: c_liabilities,
        Contacts: {
          NamePrefix: p_title,
          pyEmail1: p_emailAddress,
          pyFullName: p_contactName,
          pyPhoneNumber: p_contactNumber,
        },
        ProfitAndLoss: c_profitloses,
      },
    };
    return calData;
  }

  //======================= change state data format==============================
  changeStateData() {
    let changedata = {
      content: {
        pyAuditNote: 'Proceed to next stage after update_jitendra',
        pyChangeStageOption: 'goto',
        pyGotoStage: 'Due Diligence',
      },
    };
    return changedata;
  }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router: Router) {}

  sendToken(user: any) {
    localStorage.setItem('LoggedInUser', user);
  }
  getToken() {
    return localStorage.getItem('LoggedInUser');
  }
  isLoggedIn() {
    return this.getToken() !== null;
  }
  logout() {
    localStorage.clear();
    localStorage.removeItem('LoggedInUser');
    this.router.navigate(['/']);
  }
}

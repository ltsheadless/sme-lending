import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MaterialmoduleModule } from '../matrials/materialmodule/materialmodule.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoanComponent } from './loan/loan.component';
import { GaugeChartModule } from 'angular-gauge-chart'
import { LoanService } from '../services/loan.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from '../http.interceptor';
import { AuthService } from '../services/auth.service';
import  {MatCurrencyFormatModule} from 'mat-currency-format';
import { CircularGaugeModule } from '@syncfusion/ej2-angular-circulargauge';
import { GaugeTooltipService } from '@syncfusion/ej2-angular-circulargauge';
import { ConfirmComponent } from './confirm/confirm.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    LoanComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    MaterialmoduleModule,
    GaugeChartModule,
    MatCurrencyFormatModule,
    CircularGaugeModule,
    NgxSpinnerModule
  ],
  providers: [AuthService, LoanService, GaugeTooltipService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true,
    },
  ]
})
export class HomeModule { }

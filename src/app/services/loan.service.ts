import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoanService {
  baseUrl = environment.apiUrl;
  loancalUrl = environment.loanCalCulateUrl;
  errorMsg!: string;
  constructor(private http: HttpClient) {}

  //================ create new case ==================================
  createCase(data: any): Observable<any> {
    return this.http
      .post(`${this.baseUrl}`, data, { observe: 'response' as 'body'})
      .pipe(catchError(this.errorHandler));
  }

  //================ loan calculator ==================================
  loanCalculatorDialet(data: any): Observable<any> {
    return this.http
      .post(`${this.loancalUrl}`, data, { observe: 'response' })
      .pipe(catchError(this.errorHandler));
  }

  // ====================get all existing customer case==================
  getAllCases(): Observable<any> {
    return this.http.get(this.baseUrl, { observe: 'response' });
  }
  // ====================get existing customer case By Id==================
  getCaseById(id: any): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/${id}`, { observe: 'response' })
      .pipe(catchError(this.errorHandler));
  }

  // ====================get loan calculate==================
  createCalculateLaonById(id: any): Observable<any> {
    console.log(`${this.loancalUrl}/${id}`);
    return this.http
      .post(`${this.loancalUrl}/${id}`, {}, { observe: 'response' })
      .pipe(catchError(this.errorHandler));
  }

  // ====================get existing customer case By Id==================
  updateCaseById(id: any, data: any): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    let etag: any = localStorage.getItem('etag');
    headers = headers.append('If-Match', etag);
    console.log(headers);
    return this.http
      .put(`${this.baseUrl}/${id}`, data, { headers, observe: 'response' })
      .pipe(catchError(this.errorHandler));
  }

  // ====================get existing customer case By Id==================
  updateChangeStateById(id: any, data: any): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    let etag: any = localStorage.getItem('etag');

    headers = headers.append('If-Match', etag);

    return this.http
      .put(`${this.baseUrl}/${id}?actionID=pyChangeStage`, data, {
        headers,
        observe: 'response',
      })
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: any) {
    return throwError(error);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '../notifications/notification.service';
import { AuthService } from '../services/auth.service';
import { LoanService } from '../services/loan.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  isLinear = false;
  firstFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  userData: any;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    private loanService: LoanService,
    private router: Router,
    private notificationService: NotificationService,
    private spinner: NgxSpinnerService,
  ) {}

  ngOnInit() {
      
     /** spinner starts on init */
     this.spinner.show();

     setTimeout(() => {
       /** spinner ends after 5 seconds */
       this.spinner.hide();
     }, 3000);

    let data: any = localStorage.getItem('users');
    this.userData = JSON.parse(data);

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required],
    });
  }

  //==========================new loan create ==============================
  refreshGridInAnotherComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    console.log(this.createcaseData());
    this.router.onSameUrlNavigation = 'reload';

    this.loanService.createCase(this.createcaseData()).subscribe(
      (data) => {
        console.log(data)
        if (data.status === 201) {
          let caseTd = data.body.ID;
          this.router.navigateByUrl(`/home/loan/${caseTd}`);
        }
      },
      (error) => {
        console.log('create_data_error', error);
        let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
        this.notificationService.showError(
          `${errorMessage}`,
          'Server error occurs'
        );
      }
    );
  }

  createcaseData() {
    let caseData = {
      caseTypeID: 'CG-FW-SME-Work-LoanOrigination',
      processID: 'pyStartCase',
      parentCaseID: '',
      content: {},
    };
    return caseData;
  }

  logout() {
    this.authService.logout();
  }
}
